import { Component, OnInit,Input } from '@angular/core';
import { FormGroup,FormControl,Validators, FormArray,  } from '@angular/forms';
import {QuizzesService} from '../../../quizzes.service';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';

@Component({
  selector: 'app-redac',
  templateUrl: './redac.component.html',
  styleUrls: ['./redac.component.css']
})


export class RedacComponent implements OnInit {
  public Editor = ClassicEditor;
  questionsForm: FormGroup;
  constructor(private qService: QuizzesService) { }
  @Input() quiz;
  ngOnInit() {
    this.questionsForm= new FormGroup({
      questions: new FormArray([])
    });
    this.addQuestion();
    
  }

addQuestion(){
  let question= new FormGroup({
    question: new FormControl('',[Validators.required])
  })
  this.questions.push(question);
  
}
onSubmit(){
  this.quiz['questions']=this.questionsForm.value.questions
  this.qService.addQuiz(this.quiz)
}


get questions(): FormArray {
  return this.questionsForm.get('questions') as FormArray;
}
}
