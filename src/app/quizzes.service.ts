import { Injectable } from '@angular/core';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class QuizzesService {
quizzes=[];
  constructor(private router: Router) {
    if(localStorage.getItem('quizzes')!== null)
    this.quizzes = JSON.parse(localStorage.getItem('quizzes'));
  
  }

  addQuiz(quiz){
    this.quizzes.push(quiz);
    localStorage.setItem('quizzes', JSON.stringify(this.quizzes));
    this.router.navigateByUrl('/list')
  }

  editQuiz(){
  }

  showQuiz(){
  }

  deleteQuiz(d: number){
    this.quizzes.splice(d,1);
    localStorage.setItem('quizzes', JSON.stringify(this.quizzes));
  }
}
