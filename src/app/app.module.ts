import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {ReactiveFormsModule, FormsModule} from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { MaterialModule} from './material/material.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UsersManagementComponent } from './users-management/users-management.component';
import { QuizzesManagementComponent } from './quizzes-management/quizzes-management.component';
import { LoginComponent } from './users-management/login/login.component';
import { RegisterComponent } from './users-management/register/register.component';
import { AddQuizComponent } from './quizzes-management/add-quiz/add-quiz.component';
import { ListQuizzesComponent } from './quizzes-management/list-quizzes/list-quizzes.component';
import { EditQuizComponent } from './quizzes-management/edit-quiz/edit-quiz.component';
import { ShowQuizComponent } from './quizzes-management/show-quiz/show-quiz.component';
import { NavigationComponent } from './navigation/navigation.component';
import { AuthGuard, AuthGuard1 } from './auth.guard';
import {QcmComponent} from './quizzes-management/add-quiz/qcm/qcm.component';
import {RedacComponent} from './quizzes-management/add-quiz/redac/redac.component';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { SidbarComponent } from './sidbar/sidbar.component';

const appRoutes: Routes = [
  { path: 'login', component: LoginComponent, canActivate: [AuthGuard1]},
  { path: 'list', component: ListQuizzesComponent, canActivate: [AuthGuard]},
  { path: 'register', component: RegisterComponent},
  { path: 'add-quiz', component: AddQuizComponent,canActivate: [AuthGuard]},
  { path: 'edit-quiz/:id', component: EditQuizComponent,canActivate: [AuthGuard]},
  { path: 'show-quiz/:id', component: ShowQuizComponent,canActivate: [AuthGuard]},
  { path: '', redirectTo: 'login', pathMatch: 'full'}
];
@NgModule({
  declarations: [
    AppComponent,
    UsersManagementComponent,
    QuizzesManagementComponent,
    LoginComponent,
    RegisterComponent,
    AddQuizComponent,
    ListQuizzesComponent,
    EditQuizComponent,
    ShowQuizComponent,
    NavigationComponent,
    QcmComponent,
    RedacComponent,
    SidbarComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    RouterModule.forRoot(appRoutes),
    CKEditorModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }