import { Component, OnInit } from '@angular/core';
import {QuizzesService} from '../../quizzes.service';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl,Validators, FormArray } from '@angular/forms';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';

@Component({
  selector: 'app-show-quiz',
  templateUrl: './show-quiz.component.html',
  styleUrls: ['./show-quiz.component.css']
})
export class ShowQuizComponent implements OnInit {
  passQuizForm: FormGroup;
  public Editor = ClassicEditor;
  labels=[];
  title="";
  type="";
  passIndex: number;
  constructor(private qService: QuizzesService,private route: ActivatedRoute) { }

  ngOnInit() {
    this.passIndex = this.route.snapshot.params.id;
    this.labels=this.qService.quizzes[this.passIndex].questions;
    this.title=this.qService.quizzes[this.passIndex].title;
    this.type=this.qService.quizzes[this.passIndex].type;
     if (this.qService.quizzes[this.passIndex].type=='redac'){
      console.log('redac')
      this.passQuizForm = new FormGroup({
        answers: new FormArray([])
      })
      for(let i =0;i<this.qService.quizzes[this.passIndex].questions.length; i++){
        console.log('for Redac')
        let answer= new FormGroup({
          answer: new FormControl('',[Validators.required])
        })
        this.answers.push(answer);
      }
     } else{
       console.log('qcm')
      this.passQuizForm = new FormGroup({
        answers: new FormArray([])
      })
      for(let i =0; i<this.qService.quizzes[this.passIndex].questions.length; i++){
        console.log('ici le for qcm');
        
        let answer= new FormGroup({
          suggestion1: new FormControl(''),
          suggestion2: new FormControl(''),
          suggestion3: new FormControl(''),
          suggestion4: new FormControl('')
        })
        this.answers.push(answer);
      }
     }
    
    
    console.log(this.qService.quizzes[this.passIndex].questions);
   

  }
  get answers(): FormArray {
    return this.passQuizForm.get('answers') as FormArray;
  }
  onSubmit(){
    console.log(this.passQuizForm.get('answers'));
    
  }
}
