import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { UsersService } from './users.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    public usersService: UsersService,
    public router: Router){}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    
      if (this.usersService.isLoggedIN())
    return true
    else{
      this.router.navigateByUrl('login')
      return false
    }
    
  }
  
}


@Injectable({
  providedIn: 'root'
})
export class AuthGuard1 implements CanActivate {
  constructor(
    public usersService: UsersService,
    public router: Router){}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    
      if (this.usersService.isLoggedIN()){
      this.router.navigateByUrl('/list')
    return false
  }
    else{
      return true
    }
    
  }
  
}