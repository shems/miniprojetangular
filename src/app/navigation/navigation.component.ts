import { Component, OnInit } from '@angular/core';
import {UsersService} from '../users.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  constructor(public usersService: UsersService) { }

  ngOnInit() {
  }
  
  checkLogin(){
    return this.usersService.isLoggedIN();
  }
  logout(){
    this.usersService.logout();
  }

}
