import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuizzesManagementComponent } from './quizzes-management.component';

describe('QuizzesManagementComponent', () => {
  let component: QuizzesManagementComponent;
  let fixture: ComponentFixture<QuizzesManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuizzesManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizzesManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
