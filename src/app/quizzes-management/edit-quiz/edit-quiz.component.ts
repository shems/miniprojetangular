import { Component, OnInit } from '@angular/core';
import {QuizzesService} from '../../quizzes.service';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl,Validators, FormArray } from '@angular/forms';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';

@Component({
  selector: 'app-edit-quiz',
  templateUrl: './edit-quiz.component.html',
  styleUrls: ['./edit-quiz.component.css']
})
export class EditQuizComponent implements OnInit {
editQuizForm: FormGroup;
public Editor = ClassicEditor;
public data = "loool";
  editIndex: number;
  title="";
  constructor(private qService: QuizzesService,private route: ActivatedRoute) { }

  ngOnInit() {
    this.editIndex = this.route.snapshot.params.id;
    this.title=this.qService.quizzes[this.editIndex].title;
    this.editQuizForm = new FormGroup({
      title: new FormControl(this.title,[Validators.required]),
      type: new FormControl(this.qService.quizzes[this.editIndex].type,[Validators.required]),
      time: new FormControl(this.qService.quizzes[this.editIndex].time,[Validators.required]),
      topic: new FormControl(this.qService.quizzes[this.editIndex].topic,[Validators.required]),
      questions: new FormArray([])
    })
    for(let i =0;i<this.qService.quizzes[this.editIndex].questions.length; i++){
      let question= new FormGroup({
        question: new FormControl('',[Validators.required])
      })
      this.questions.push(question);
    }
    console.log(this.editQuizForm)
  }
  get questions(): FormArray {
    return this.editQuizForm.get('questions') as FormArray;
  }
  onSubmit(){

  }
}
