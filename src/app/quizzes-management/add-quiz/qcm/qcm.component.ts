import { Component, OnInit,Input } from '@angular/core';

import { FormGroup,FormControl,Validators, FormArray,  } from '@angular/forms';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {QuizzesService} from '../../../quizzes.service';

@Component({
  selector: 'app-qcm',
  templateUrl: './qcm.component.html',
  styleUrls: ['./qcm.component.css']
})
export class QcmComponent implements OnInit {
  public Editor = ClassicEditor;
  questionsForm: FormGroup;
  constructor(private qService: QuizzesService) { }

  @Input() quiz;
  ngOnInit() {
    this.questionsForm= new FormGroup({
      questions: new FormArray([])
    });
    this.addQuestion();
    
  }

addQuestion(){
  let question= new FormGroup({
    question: new FormControl('',[Validators.required]),
    suggestion1: new FormControl('',[Validators.required]),
    suggestion2: new FormControl('',[Validators.required]),
    suggestion3: new FormControl('',[Validators.required]),
    suggestion4: new FormControl('',[Validators.required]),
    rightAnswer: new FormControl('',[Validators.required])
  })
  this.questions.push(question);
  
}
  onSubmit(){
    
    this.quiz.questions=this.questionsForm.value.questions;
    this.qService.addQuiz(this.quiz)
  }
  get questions(): FormArray {
    return this.questionsForm.get('questions') as FormArray;
  }

}
