import { Component, OnInit } from '@angular/core';

import {UsersService} from '../users.service';

@Component({
  selector: 'app-sidbar',
  templateUrl: './sidbar.component.html',
  styleUrls: ['./sidbar.component.css']
})
export class SidbarComponent implements OnInit {

  constructor(private usersService: UsersService) { }

  ngOnInit() {
  }
  checkLogin(){
    return this.usersService.isLoggedIN();
  }
}
