import { Component, OnInit } from '@angular/core';
import {FormGroup, Validators, FormControl} from '@angular/forms';
import { UsersService} from '../../users.service'
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private usersService: UsersService) { }
  RegisterForm: FormGroup;

  ngOnInit() {
    this.RegisterForm= new FormGroup({
      userName: new FormControl('',[Validators.required]),
      email: new FormControl('',[Validators.required, Validators.email]),
      pwd: new FormControl('',[Validators.required]),
      cpwd: new FormControl('',[Validators.required]),
      isCoach: new FormControl(false),
      terms: new FormControl(false,[Validators.required])
    })
  }

  onSubmit(){
    delete this.RegisterForm.value.terms;
    delete this.RegisterForm.value.cpwd;
    this.usersService.addUser(this.RegisterForm.value);
  }

}
