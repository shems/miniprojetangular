import { Injectable } from '@angular/core';
import {Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  users=[
    {userName: "shems",email: "shems@shems.com", pwd:'shems', isCoach: false}];
    currentUser;

  constructor(private router: Router) { 
    if(localStorage.getItem('user'))
    this.currentUser = JSON.parse(localStorage.getItem('user'));
    else this.currentUser = "";
  }
  addUser(user){
    this.users.push(user);
    
  }

  login(user){
    localStorage.setItem('user', JSON.stringify(user));
    this.currentUser= user;
    this.router.navigateByUrl('/list')
  }

  logout(){
    localStorage.setItem('user', JSON.stringify(''));
    this.currentUser= "";
    this.router.navigateByUrl('/login');
  }

  isLoggedIN(){
    if (this.currentUser!=='')
    return true
    else{
      return false
    }
  }
}
