import { Component, OnInit } from '@angular/core';
import {FormGroup, Validators, FormControl} from '@angular/forms';
import {UsersService} from '../../users.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;

  constructor(private usersService: UsersService) { }

  ngOnInit() {
    this.loginForm= new FormGroup({
      userName: new FormControl('',[Validators.required]),
      pwd: new FormControl('',[Validators.required])
    })
  }
onSubmit(){
  console.log('login submit');
  
this.usersService.login(this.loginForm.value);
}
}
