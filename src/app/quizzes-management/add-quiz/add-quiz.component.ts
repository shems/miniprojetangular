import { Component, OnInit } from '@angular/core';
import { FormGroup,FormArray, FormControl,Validators, FormArrayName } from '@angular/forms';
import {QuizzesService} from '../../quizzes.service'


@Component({
  selector: 'app-add-quiz',
  templateUrl: './add-quiz.component.html',
  styleUrls: ['./add-quiz.component.css']
})
export class AddQuizComponent implements OnInit {
  quizForm: FormGroup;
  quizType="";

  constructor(private qService: QuizzesService) { }
  

  ngOnInit() {
    this.quizForm= new FormGroup({
      title: new FormControl('',[Validators.required]),
      time: new FormControl(60,[Validators.required]),
      type: new FormControl('',[Validators.required]),
      topic: new FormControl('',[Validators.required])
    });
  }
  
  confirmQuizType(){
    this.quizType= this.quizForm.value.type;
  }
  
  
}
