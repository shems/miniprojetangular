import { Component, OnInit } from '@angular/core';
import {QuizzesService} from '../../quizzes.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-list-quizzes',
  templateUrl: './list-quizzes.component.html',
  styleUrls: ['./list-quizzes.component.css']
})

export class ListQuizzesComponent implements OnInit {
  quizzes= this.qService.quizzes;
  constructor(private qService: QuizzesService) { }
  ngOnInit() {
    
  }
  
  delete(d: number){
  Swal.fire({
    title: 'Are you sure?',
  text: "You won't be able to revert this!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonText: 'Yes',
  cancelButtonText: 'Cancel',
  reverseButtons: true
  }).then((result) => {
    if (result.value) {
      this.qService.deleteQuiz(d);
    } else if (
      /* Read more about handling dismissals below */
      result.dismiss === Swal.DismissReason.cancel
    ) {
      
    }
  })
}
}
